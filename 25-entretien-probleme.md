---
author: Thomas Clavier
title: L'entretien orienté problème
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg) |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>


# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous appris ?
* Qu'avez-vous fait ?

# L'entretien problème

![](../includes/3-steps-step-1.1.svg)

# Les pièges à éviter

* Les enquêtes en ligne
* Les Focus Groupes
* Je connais le Client
* Développer le produit pour avoir du retour
* Parler plus au BA et Vcs qu'aux Clients
* Sous-traiter les entretiens clients

# Prospects à interroger

* Interrogez seulement les personnes dans votre cible
* Commencez dans votre entourage
* Demandez à être mis en contact
* Utilisez les réseaux sociaux
* Construisez une landing page, récoltez des contacts (email, téléphone)

# Script {.small}

| | | |
|-|-|-|
|Accueillir | 2 min | Préparer le terrain |
|Recueillir les données personnelles | 2 min | Tester le segment de clientèle |
|Raconter une histoire | 2 min | Définir le contexte du problème |
|Établir un classement des problèmes | 5 min | Tester les problèmes |
|Découvrir le point de vue du client | 15 min | Apprendre sur les problèmes |
|Terminer l’entretien | 2 min | la permission de le contacter de nouveau et de vous présenter à d’autres personnes |
|Consigner les résultats | 5 min | |


# Accueillir

* Remerciez le client pour l'entretien
* Expliquez que vous menez des recherches sur une idée de nouveau produit
* Décrivez le process

# Recueillir les données personnelles

* Assurez-vous que le client est bien dans la cible
* Identifiez les caractéristiques de vos early adopters
    * Depuis combien de temps existe votre socièté ?
    * Combien avez-vous d'enfants ?
    * Utilisez-vous les réseaux sociaux ?
    * A quelle fréquence ?

# Raconter une histoire

* Mettre du contexte pour présenter les 3 principaux problèmes
* Déclencher de l'empatie chez votre interlocuteur

# Établir un classement des problèmes

* Résumez les 3 principaux problèmes
* Est-ce que ces problèmes vous parlent ?
* Est-ce qu'ils existent d'autres problèmes dont je n'ai pas parlé
* Comment classeriez-vous ces problèmes ?
* On peut utiliser des fiches papier pour présenter les problèmes et les faire classer

# Découvrir le point de vue du client

* Commencez par le problème N°1
* Comment gérez-vous ce problème aujourd'hui ?
* Cherchez à savoir si et comment ils résolvent ce problème ?
* Répétez avec les autres problèmes

# Terminer l’entretien

* Demandez si vous pourrez le recontacter une fois que vous aurez la solution
* Demandez qui d'autre de son entourage vous pourriez interroger
* Fait il partie d'un groupe d'échange entre pair sur son métier ? si oui, lequel ?

# Documenter l'entretien

* Retranscrivez immédiatement les résultats
* Créez un modèle de la cible
* Utilisez une suite CRM ou un fichier client (attention déclaration CNIL)

# 

![](../includes/canvas-entretien-probleme.svg)

# Pour la prochaine fois.

Élaborer un l'entretien problème et le jouer avec au moins 10 personnes.

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-azae.svg){height=100px}

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

:::
::::

Support de formation créé par [Efidev](https://www.efidev.com/) et [![](../includes/logo-azae.svg){height=1.5em}](https://azae.net) sous licence [![](../includes/cc-by-sa.svg){height=1em}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

