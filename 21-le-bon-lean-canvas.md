---
author: Thomas Clavier
title: Choisir son Lean Canvas
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg) |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>


# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Choisir son Lean Canvas

![](../includes/3-steps-step-0.svg)

# Risques et Incertitudes

Selon Frank Knight (1885-1972), économiste et auteur de la théorie du risque :

* Le risque désigne une situation où les possibilités de l'avenir sont connues et probabilisables.
* l'incertitude désigne une situation où l'on ignore tout cela, où non seulement l'avenir n'est pas connu, mais il ne peut l’être.

# Dérisquer son projet

# Identifier les risques

* Risque **Produit** : Concevoir un bon produit
* Risque **Client** : Créer un lien avec les clients
* Risque **Marché** : Créer une activité viable

# Identifier les risques

![](../includes/lean-canvas-fr-types-risques.svg){height=500px}

# Choisir

* Évaluer les business models pour prioriser et choisir par quoi commencer
* Choisir le modèle avec un marché suffisamment grand pour atteindre des clients qui ont besoin de la solution et pour lequel on peut construire un business.

# Atelier

Pour identifier le lean canvas du business model le moins risqué, nous allons les prioriser.

* l'importance du problème pour le client
* la facilité à atteindre le client
* le potentiel économique
* la taille du marché
* la faisabilité technique

> Avis externe d'un professionnel adressant la même cible pour évaluation des risques.

# Team Poker

![](../includes/team-poker.jpg){height=400px}

![](../includes/poker.png){height=50px}
[![](../includes/f-droid.png){height=50px}](https://play.google.com/store/apps/details?id=saschpe.poker)
[![](../includes/google-play.png){height=50px}](https://f-droid.org/en/packages/saschpe.poker/)

# Le jeu

* On prend l'hypothèse que 5 est un risque moyen.
* Ce sont des risques relatifs.
* Pour chaque sujet, en même temps, tous les participants montrent la carte de leur choix.
* Les personnes qui ont mis le plus petit chiffre et le plus grand chiffre, expliquent les raisons de leur choix.
* Les participants revotent une seconde fois, en intégrant les explications de chacun.
* On fait la moyenne des points pour avoir le nombre de points.

# Lean canvas

![](../includes/lean-canvas-fr-notes-risques.svg){height=500px}

# Choisir le business model le moins risqué pour commencer.

# Feedback loop

![](../includes/feedback.svg)

# Lean Startup Board

![](../includes/lean-dashboard-sample.jpg){height=500px}

# Lean Startup Board

![](../includes/lean-startup-board-fr.svg){height=500px}

# 

Objectif : valider un lean canvas dans son ensemble.

![](../includes/lean-startup-board-annote.svg){height=400px}

# Expériences

Objectif : valider chacune des hypothèses de votre lean canvas.

On commence par l'hypothèse la plus risqué.

* À quoi verrez vous que l'hypothèse est juste ?
* Objectif SMART (Specific, Measurable, Assignable, Realistic, Time-related) et observable.
* Quels sont les différentes expériences que vous pouvez menez pour valider ou invalider cette hypothèse ?
* Mener les expérience 1 à 1 pour valider ou invalider l'hypothèse.

# Indicateurs

* À quoi verrez vous que votre solution correspond bien au problème ?
* À quoi verrez vous que votre produit a bien son marché ?
* À quoi verrez vous que votre entreprise sera un succès ?

# Customer Factory

![](../includes/customer-factory.svg){height=300px}

> Votre business est une usine qui fabrique des clients heureux.

# Pirates Metrics

![](../includes/pirates-metrics.svg)

# Customer Factory

![](../includes/customer-factory-pirates.svg){height=400px}

# Expérimenter

N’expérimenter qu’une hypothèse à la fois

* Identifier l’hypothèse à valider ou invalider
* Formuler votre hypothèse réfutable et identifier les indicateurs
* Identifier au moins 3 expériences différentes pour valider ou invalider cette hypothèse
* Réaliser une des expériences
* Valider qualitativement, vérifier quantitativement
* Mesurer
* Communiquer les résultats
* Analyser, pivoter ou persévérer.

# Lean sprint

Le succès d’une démarche Lean Startup se joue sur l'adoption de pratiques mais aussi sur la tenue du rythme des boucles d’apprentissage.

* itérations de 1 à 2 semaines sur un principe proche du sprint du framework agile Scrum,
* avec des cérémonies de planification, de points quotidiens de coordination et de revue de sprint.
* La durée fixe (time box) est utile pour encourager la prise de décision

# Lean sprint

1. Soumettre les problèmes : Identifier la contrainte, l’hypothèse ?.
2. Définir les solutions: Déterminer comment lever cette contrainte, vérifier l’hypothèse?
3. Lister les solutions: Sélectionner les meilleures stratégies.
4. Tester les solutions: Tester ces stratégies avec les expériences.
5. Décider des solutions: Décider des actions à venir.

# Mesurer

Objectif

* Etre capable de visualiser et mesure le cycle de vie client
* Mesurer ce que font vraiment les clients
* Reporter régulièrement pour mesurer les progrès

# Mesurer

Activités

* Définir les métriques
* Acquisition - Activation - Rétention - Revenu - Référencement
* Mettre en place la plate-forme analytics (développement?)
* Mettre en place et produire le Tableau de bord de conversion

# Mesurer

Vos outils

* Intégration de la plate-forme analytics par cohorte
* Tableau de bord de conversion

# Mesurer

**Actionable metrics**, un indicateur concret, pragmatique, une mesure qui lie actions spécifiques et reproductibles aux résultats observés.

vs 

**Vanity Metrics** / Indicateurs de suffisance ( ex : Nb visites Web ou nombre de téléchargement)

# 3 règles

* Choisir la bonne métrique
* Créer des rapports simples
* Derrière les indicateurs se cachent des gens

# Atelier

* Trouver les pirates métrics adéquation marché / produit de votre lean canvas
* Créer le rapport correspondant

# adéquation marché / produit

Exemple : 

* Activation (S’inscrire, Télécharger, Publier du contenu, Partager a un ami)
* Rétention (Revisites, Abandon, Fidélité, Fonctionnalité principale)

# Rapport

Simple, Visuel, Cartographie le flux d’activation

ex : Entonnoir de conversion : 

| | | |
|-|-|-|
|Se connecte                    | 3000 | 100%|
|Télécharge                     | 2400 | 80% |
|Utilise la fonction principale | 1500 | 50% |
|Achète                         | 210  | 7%  |

# Les cohortes

> Une cohorte désigne un ensemble d'individus ayant vécu un même événement au cours d'une même période. 

Wikipédia

# Cohortes

Vos exépriences produisent de facto des cohortes (réalisation d'une expérience, A/B testing, etc.)

|Cohorte 1 | Cohorte 2 | Cohorte 3 |
|-|-|-|
|Se connecte                    | Se connecte                    |Se connecte                    |
|Télécharge                     | Télécharge                     |Télécharge                     |
|Utilise la fonction principale | Utilise la fonction principale |Utilise la fonction principale |
|Achète                         | Achète                         |Achète                         |


# Rapport de cohortes

| |Cohorte 1 | Cohorte 2 | Cohorte 3 |
|-|-|-|-|
|Se connecte                    | 3000 | 2000 | 6000 |
|Télécharge                     | 2400 | 2200 |      |
|Utilise la fonction principale | 1500 |  |
|Achète                         | | |

# Rapport de cohortes

Le temps se déroule sur les 2 axes

* les actions du prospect
* les cohortes

# Mesurer la rétention 

::: {.small}

| | Janvier | Février | Mars | Avril | Mai | Juin | Juillet |
|-|---------|---------|------|-------|-----|------|---------|
|Semaine 1 | 100% | 10% | 9% | 9% | 7% | 7% | 7% |
|Semaine 2 | 100% | 12% | 10%|10% | 8% | 7% |
|Semaine 3 | 100% | 16% | 14%|13% |12% |    |
|Semaine 4 | 100% | 17% | 15%|14% |    |    |
|Semaine 5 | 100% | 20% | 17%| |    |    |

:::

# Mesurer la conversion

![](../includes/mesure-conversion.jpg){height=500px}

# Derrière se cachent des gens

Collectez les moyens de les contacter en cas d'échec comme en cas de succes.

# Les outils

* Google Analytics
* Piwik
* Kissmetrics
* Mixpanel
* etc.

# Mise en application

# Comprendre le problème

![](../includes/3-steps-step-1.1.svg)

# Expérimenter : Objectifs

* Identifier le profil du client Early Adopter
* Avoir identifié le problème principal
* Pouvoir définir les fonctionnalités minimales pour résoudre ce problème
* Connaître le prix que le client est prêt à payer
* Pouvoir construire un vrai business avec tout ça

# Experimenter : Outil

* Lean Dashboard (Lean stack - Ash Maurya)

# Comprendre le problème

* Risque produit : Quel est le problème à résoudre ?
    * Quel est le top 3 des problèmes ?
* Risque client : Qui a le problème ?
    * Est-ce que le segment client est viable ?
* Risque Marché : Quelle est la concurrence ?
    * Comment les clients résolvent le problème aujourd'hui ?

# Comprendre le problème : Outils

* Planning d'interviews
* Le Validation Board
* Guide d'interview et Compte-rendus
* Lean Canvas mis à jour
* Teaser landing page

# Comprendre le problème : Activités

* Identification des hypothèses
* Préparation des interviews
* Recrutement et interviews des prospects
* Mise à jour du Lean canvas
* Pivots éventuels

# Comprendre le problème : Critères de sortie

* Trouver au moins 10 personnes et être capable
    * d'identifier le profil du early adopter
    * d'avoir cerné le problème principal
    * de décrire comment les prospects résolvent le problème aujourd'hui.


# Pour la prochaine fois.

Élaborer un planning d’expérimentation

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-azae.svg){height=100px}

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

:::
::::

Support de formation créé par [Efidev](https://www.efidev.com/) et [![](../includes/logo-azae.svg){height=1.5em}](https://azae.net) sous licence [![](../includes/cc-by-sa.svg){height=1em}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

