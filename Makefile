REVEAL_JS_VERSION=3.8.0
REVEAL_JS_DIR=reveal.js-$(REVEAL_JS_VERSION)
FORK_AWESOME_VERSION=1.1.7
FORK_AWESOME_DIR=Fork-Awesome-$(FORK_AWESOME_VERSION)
SLIDES_SRC = $(wildcard *.md)
CLEAN=*~ *.rtf *.ps *.log *.dvi *.aux *.out *.html *.bak *.toc *.pl *.4ct *.4tc *.lg *.sxw *.tmp *.xref *.idv *.tns
SVG_FILES= $(wildcard includes/*.svg)

all: public/index.html slides

reveal.js.zip:
	wget -q -O reveal.js.zip https://github.com/hakimel/reveal.js/archive/$(REVEAL_JS_VERSION).zip

fork-awesome.zip:
	wget -q -O fork-awesome.zip https://github.com/ForkAwesome/Fork-Awesome/archive/$(FORK_AWESOME_VERSION).zip

public/$(REVEAL_JS_DIR): reveal.js.zip
	unzip -o reveal.js.zip -d public

public/$(FORK_AWESOME_DIR): fork-awesome.zip
	unzip -o fork-awesome.zip -d public

public/index.html: public/$(REVEAL_JS_DIR) public/$(FORK_AWESOME_DIR) README.md
	mkdir -p public
	cp -r includes public/
	cp *.css public/
	pandoc --css=./azae.css --css=./index.css --standalone -f markdown+smart < README.md > public/index.html

public/%: %.md 
	mkdir -p $@
	pandoc -t revealjs --slide-level=1 -s -o $@/index.html -f markdown+smart $< -V revealjs-url=../$(REVEAL_JS_DIR)/ -V theme=white --css=../azae.css

public/%.pdf: %.svg
	cp $< public/includes/
	inkscape --export-type="pdf" --export-filename="$@" $< 

public/%.png: %.svg
	cp $< public/includes/
	inkscape --export-type="png" --export-filename="$@" $<

clean:
	rm -rf $(CLEAN) public

slides: $(patsubst %.md, public/%, $(SLIDES_SRC)) $(patsubst %.svg, public/%.png, $(SVG_FILES)) $(patsubst %.svg, public/%.pdf, $(SVG_FILES))

ci:
	inotify-hookable $(patsubst %, -f %, $(SLIDES_SRC)) $(patsubst %, -f %, $(SVG_FILES)) -f azae.css -w includes  -c "make"
