---
author: Thomas Clavier
title: Définir et valider son Produit Minimum Viable
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg) |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Définir et valider son MVP

![](../includes/3-steps-step-2.svg)

# Adéquation Produit/Marché

# Développer

* Les Early adopters sont identifiés et identifiables
* Le problème essentiel est identifié
* Le prix de vente est validé
* Le modèle économique est théoriquement viable
* GET TO RELEASE 1.0 - MVP

# MVP / MVE

* Minimum Viable Product - Minimim Valuable Product : l’ensemble minimum de fonctionnalités qui créent de la valeur.
* Minimum Valuable Experience : la plus petite expérience utilisateur qui permet d'apprendre quelques chose dans le but de créer le la valeur.

# Origine

Créée par Frank Robinson en 2001, popularisée par Éric Ries et Steve Blank.

# Types de MVP

# Video d'explication

Une vidéo expliquant les nouvelles fonctionalités du produit.

# Landing Page

Une page web vantant les bénéfices du produit, invitant l'utilisateur à laisser ses coordonnés.

# Magicien d'Oz

Simuler le tout automatisé, sans savoir si c'est techniquement faisable.

# Concierge

Faire les tâches simples et rébarbatives à la main.

# Fragmentaire (Piecemeal)

Tirer profit des plateformes existantes.

# Levée de fonds Clients

Commencer par récolter des fonds et des engagements sur une plateforme de crowdfunding.

# Fonctionnalité unique

À la google à ses débuts, juste un moteur de recherche.

# Pré-commande

À rapprocher de la levée de fonds clients.

# Low tech

Le cardboard.

# Hardware

# Minimum Viable Product

# Objectif

* Développer la solution tout en apprenant
* Mettre en place le cycle de validation qualitative
* Définir la solution minimale viable : réduire le périmètre et ne garder que le "Must Have"
* Focus sur l'apprentissage
* Développement de la solution
* Développement du client

# Activités

* Élaborer le backlog
    * Fonctionnalités incontournables identifiées
    * Le flux d'activation
    * Le site web marketing
* Développement agile de la solution minimale viable
* Cadre d’organisation recommandé : kanban
    * Déploiement continu des produits ou servives numériques (simple)

# Vos outils

* User Story Mapping
* Le Backlog
* Le questionnaire Kano
* Process projet Kanban
* La produit en Release 1.0

# User Story Mapping

* Le user story mapping a été inventé par Jeff Patton, en 2008, à Salt Lake City, Utah
* Un atelier qui permet de créer le product backlog, en travaillant sur un outil de marketing, afin de partager la vision d'un produit

# {data-background-image="../includes/story-map.jpg"}


# Épopées

* Dérouler les grands thèmes sous forme d'une histoire linéaire
* Détailler chaque thème en épopées

![](../includes/story-map-1.svg){height=400px}

# Histoires utilisateurs

* Détailler les épopées en histoires utilisateurs

![](../includes/story-map-2.svg){height=400px}

# Versions

* Regrouper les histoires utilisateurs en MVP/MVE

![](../includes/story-map-3.svg){height=400px}

# sk8

* Objectif : la ou les fonctionnalités de la version skateboard
* Itération 1 / Sprint 1
    * US
    * US ...
* Itération 2 / Sprint 2
* Itération 3 / Sprint 3

# La revue de backlog

* Passage en revue des prochaines itérations
* Réorganisation des sujets en fonction
    * de l’actualité
    * des opportunités
    * du niveau de préparation
    * des découvertes
    * des retours clients
* Vérification de la cohérence des sprints

# {data-background-image="../includes/revue-backlog.jpg"}

# Le management visuel

# Kanban

# Site web marketing

* Acquisition
* Unique Value Proposition (landing page)
* Autres Pages (Tour, About Us)
* Pricing
* Click Sign-up

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-azae.svg){height=100px}

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

:::
::::

Support de formation créé par [Efidev](https://www.efidev.com/) et [![](../includes/logo-azae.svg){height=1.5em}](https://azae.net) sous licence [![](../includes/cc-by-sa.svg){height=1em}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

