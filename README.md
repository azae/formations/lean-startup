---
title: Lean Startup
---

Une formation au Lean Startup en 6 demis journées espacées de 3 semaines.

## Introduction au Lean Startup

* Objectif : présenter les fondements du lean startup et la nécéssité de rythmer.
* Déroulé : 
    * 1h de prez
    * 1h de snowflake
    * 1h de débrief + retour sur les slides
    * 30 min de prez des ateliers suivants
* [Slides](./01-introduction/)

## Définir son modèle économique et documenter sa vision avec avec le Lean Canvas

* Objectif : travailler le lean canvas et l'utiliser comme point de départ pour pitcher son projet
* Déroulé :
    * 2h en binôme tournant pour faire les lean canvas (apprendre par l'exemple) On tourne toutes les 20 min. Intercaller les moments théoriques et lean canvas
    * 1h pour transformer un lean canvas en pitch en binôme
* [Slides Business Model](./11-business-model/)
* [Slides Lean Canvas](./12-lean-canvas/)
* [Slides Pitch Deck](./13-pitch-deck/)
* [Lean canvas à imprimer](./includes/lean-canvas-fr.pdf)

## Dérisquer son projet et mesurer sa réussite

* Objectif : choisir le bon lean canvas
* Déroulé : 2 groupes de 4 ou 5 avec les leans canvas d'un des porteurs puis par séquences de 20min
    * Présentation risque poker
    * Atelier risque poker => on sélectionne 1 lean canvas
    * Présentation générale du lean startup board
    * Atelier trouver des expériences pour chaque hypothèses du lean canvas
    * Présentation pirates métrics
    * Atelier trouver les bonnes métriques
    * Construire un rapport de suivi
    * Voir la notion de cohortes
    * Faire évoluer le rapport pour intégrer les cohortes
* [Slides Choisir le bon lean canvas](./21-le-bon-lean-canvas/)
* [Slides Entretien problème](./25-entretien-probleme/)
* [Lean startup board à imprimer](./includes/lean-startup-board-fr.pdf)
* [Canvas entretien problème à imprimer](./includes/canvas-entretien-probleme.pdf)

## Étudier besoin marché / segment client

* Objectif : adéquation problème / solution. Value proposition canvas depuis Lean Canvas
* Déroulé : 
    * Présentation rapide du "value proposition canvas"
    * Atelier : 20 min pour construire la partie : profile des clients
    * théorisation sur la partie "profile client"
    * Atelier : 20 min pour construire la partie : valeur
    * théorisation sur la partie valeur
    * Atelier : comment utiliser ce canvas pour exprimer sa proposition de valeur
    * montrer le template
    * Atelier : en groupe, créer sa proposition de valeur depuis son canvas
    * conclusion theorique sur le sujet puis présentation script entretient solution
    * Atelier : construire un script d'entretient solution
* [Slides value proposition canvas](./31-value-proposition-canvas/)
* [Slides Entretien Solution](./32-entretient-solution/)
* [Value proposition canvas à imprimer](./includes/value-proposition-design-fr.pdf)
* [Canvas entretien solution à imprimer](./includes/canvas-entretien-solution.pdf)

## Définir et valider son Produit Minimum Viable

* Objectif : MVP + matrice de kano
* Déroulé : 
    * En sous groupe, c'est quoi un MVP ? donnez moi des exemples.
    * Présentation rapide des slides sur le MVP
    * Atelier : story map avec échange à interval régulier, perfection game, etc.
    * Présentation des slides story map.
    * Atelier : faire le backlog de son site marketing
    * 
* [Slides Produit Minimum Viable](./41-mvp/)
* [Slides Matrice de Kano](./42-matrice-de-kano/)
* [Slides Entretien MVP](./43-entretien-mvp/)
* [Canvas entretien MVP à imprimer](./includes/canvas-entretien-mvp.pdf)

## Trouver le moteur de croissance pour valider l’adéquation produit/marché

* Objectif : KPI, adéquation produit / marché
* Déroulé :
    * Atelier : type de moteur de croissance
    * Atelier : Choisir son moteur de croissance
    * Atelier : influenceurs
    * Pirate metrics
    * En sous groupes : atelier board metrics
    * Derniers slides
* [Slides](./51-kpi/)

# Bibliographie

* The Four Steps to the Epiphany -- _Steve Blank_
* Le Manuel du créateur de start-up -- _Steve Blank et Bob Dorf_
* The Lean Startup -- _Eric Ries_
* The Leader's Guide -- _Eric Ries_
* Running Lean -- _Ash Maurya_
* Scaling Lean -- _Ash Maurya_
* Business Model Generation -- _Alexander Osterwalder_
* Value Proposition Design -- _Alexander Osterwalder_
