---
author: Thomas Clavier
title: Business models
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg) |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Business models

![](../includes/3-steps-step-0.svg)

# Business Model vs Business Plan

* Le business plan est le document dans lequel on présente le business model ou modèle économique.
* Le business plan ou plan d’affaire présente la stratégie de l’entreprise et ses implications financières pour les années à venir.
* Le business model décrit quelle proposition de valeur la startup apporte à ses clients, comment elle se positionne sur son marché et comment elle organise ses relations avec ses clients, fournisseurs, et partenaires afin de générer un profit.

# Les types de business models

# Production {data-background-image="../includes/boulangerie.jpg" data-state="white70"}

L'entreprise vend un produit ou un service qu'elle produit.

# Publicité {data-background-image="../includes/publicite.jpg" data-state="white80"}

L'entreprise génère des revenus en vendant de l'espace publicitaire à des annonceurs.

# Commission {data-background-image="../includes/commission.jpg" data-state="white90"}

L'entreprise agit en tant qu'intermédiaire entre l'acheteur et le vendeur.  Elle se rémunère en prenant une commission sur les revenus qu'elle permet de générer pour le vendeur.

# Abonnement {data-background-image="../includes/abonnement.png" data-state="white70"}

L'entreprise perçoit des revenus à intervalle régulier de ses abonnés.

# Vente de consommables {data-background-image="../includes/consommables.jpg" data-state="white70"}

L'entreprise propose un produit à un prix proche de son coût de revient et se rémunère sur la vente de consommables ou d’accessoires.

# Dégroupage {data-background-image="../includes/degroupage.png" data-state="white80"}

Basé sur le principe qu'il n'existe que trois modèles économiques : la relation client, l’innovation produit ou l’infrastructure. Le dégroupage consiste à se concentrer sur un des trois.

# Longue traîne {data-background-image="../includes/longue-traine.jpg" data-state="white80"}

L'entreprise vend un grand nombre de produits, chacun vendu en petites quantités

# Plate-forme multifaces {data-background-image="../includes/multifaces.png" data-state="white80"}

L'entreprise met en contact deux groupes de clients distincts mais interdépendants. Elle crée de la valeur en rendant possible les interactions entre les deux groupes.

# Gratuit {data-background-image="../includes/gratuit.jpg" data-state="white80"}

L'entreprise fait bénéficier d’une offre gratuite de manière continue à au moins un segment important de clients. Le financement est réalisé par une autre composante du business model ou un autre segment de clients.

# Freemium {data-background-image="../includes/nextcloud.png" data-state="white80"}

Dans le modèle freemium, une base importante bénéficie d’une offre gratuite. Seule une petite proportion (~10%) souscrits aux services Premium.

# Open Business Model {data-background-image="../includes/marque-blanche.jpg" data-state="white80"}

L’entreprise vend les résultats de sa R&D à des partenaires extérieurs qui assurent la production et la commercialisation.

# Documenter la vision

[Le lean canvas](../12-lean-canvas/)

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-azae.svg){height=100px}

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

:::
::::

Support de formation créé par [Efidev](https://www.efidev.com/) et [![](../includes/logo-azae.svg){height=1.5em}](https://azae.net) sous licence [![](../includes/cc-by-sa.svg){height=1em}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

