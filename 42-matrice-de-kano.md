---
author: Thomas Clavier
title: Matrice de Kano
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg) |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# Matrice de kano

![](../includes/3-steps-step-2.1.svg)

# Test de la solution minimale viable

# Objectif

Interview des earlyadopters pour tester avant le lancement

# Risque Produit

> Qu'est-ce qui rend la solution convaincante ?

* Est-ce que les landing pages sont vues ?
* Est-ce que les prospects suivent tout le processus d'activation ?
* Quels sont les principaux usages de la solution ?
* Est-ce que la solution démontre et tient la promesse ?

# Risque Client

> Y-a-t-il suffisamment de clients via les canaux existants ?

* Peut-on attirer encore plus de clients via les canaux existants ?

# Risque Marché : 

> Est-ce que le prix est le bon ?

* Est-ce que les clients achètent la solution ?

# Activités

* Identification des hypothèses
* Préparation les interviews MVP
* Réalisation des interviews MVP
* Obtenir le feedback sur le MVP
* Mise à jour du Lean canvas
* Mise à jour du backlog

# Vos outils

* Planning des interviews
* Guide d'entretiens et Compte-rendus
* Le questionnaire Kano
* Dashboard des apprentissages / Validation Board et Lean Canvas mis à jour
* Backlog mis à jour

# Obtenir le feedback des clients

# Le modèle de Kano

:::: {.columns}
::: {.column width="30%"}
![](../includes/noriaki-kano.jpg)
:::
::: {.column width="70%"}
* Inventé en 1984 par le Dr Noriaki Kano, professeur en management de la qualité à l’Université de Tokyo
* Mesurer la satisfaction client, de comprendre leurs attentes et besoins et d’évaluer les performances d’un produit.
* Le principe d’analyse : Une caractéristique du produit/service qui entraîne la satisfaction du client n'entraîne pas forcément d'insatisfaction du client en cas d'absence de cette caractéristique.
:::
::::

#

![](../includes/matrice-kano.svg){height=600px}

# Questionnaire

Pour chaque fonctionnalité, critère ou caractéristique du produit / service, le questionnaire comporte une question et une contre question

# Présent

:::: {.columns}
::: {.column width="50%"}
Si la fonctionnalité .......... est présente. 
:::
::: {.column width="50%"}
* Cela vous plait beaucoup 
* Vous trouvez ça normal   
* Ça vous est égal         
* Vous vous en contentez   
* Cela vous déplait        
:::
::::

# Absent

:::: {.columns}
::: {.column width="50%"}
Si la fonctionnalité .......... est absente. 
:::
::: {.column width="50%"}
* Cela vous plait beaucoup 
* Vous trouvez ça normal   
* Ça vous est égal         
* Vous vous en contentez   
* Cela vous déplait        
:::
::::

# Exemple {class=small}

|Fonction | Si elle est présente | Si elle est absente |
|---------|----------------------|---------------------|
|Courte présentation du jeu | C'est normal     | C'est normal     |
|Vidéo de démo du jeu       | C'est normal     | Ça me déplait    |
|Présentation du gameplay   | Ça me plait      | Je m'en contente |
|Présentation du lore       | Ça me plait      | Je m'en contente |
|Art zone                   | Ça me plait      | Je m'en contente |
|Inscription version Alpha  | Ça me plait      | Ça me déplait    |
|Lien vers réseau sociaux   | Ça m'est égal    | Ça me déplait    |
|Présentation de l’équipe   | Je m'en contente | Je m'en contente |
|Blog                       | Ça m'est égal    | Ça me déplait    |
|Mentions légales           | Je m'en contente | Ça m'est égal    |

# Grille d’analyse

![](../includes/matrice-kano-lecture.svg)

# 

![](../includes/matrice-kano-exemple.svg){height=600px}

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-azae.svg){height=100px}

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

:::
::::

Support de formation créé par [Efidev](https://www.efidev.com/) et [![](../includes/logo-azae.svg){height=1.5em}](https://azae.net) sous licence [![](../includes/cc-by-sa.svg){height=1em}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

