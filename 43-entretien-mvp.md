---
author: Thomas Clavier
title: Entretien MVP
---

#

| | |
|-|-|
| Thomas Clavier | ![](../includes/thomas-clavier.jpg) |


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

</small>

# Le cadre

![](../includes/cadre.svg)

# Depuis la dernière fois

* Qu'avez-vous fait ?
* Qu'avez-vous appris ?

# L’entretien MVP

![](../includes/3-steps-step-2.1.svg)

# Script {.small}

| | | |
|-|-|-|
|Accueillir | 2 min | Préparer le terrain |
|Présenter la proposition de valeur unique | 2 min | Tester la proposition de valeur unique |
|Présenter du prix | 15 min | Tester la solution |
|Inscription et activation | 15 min | Tester la solution |
|Questions de Kano | 10 min | Identifier les prochaines fonctionalités |
|Terminer l’entretien | 5 min | Maintenir la boucle de feedback active |
|Consigner les résultats | 5 min | |

# Accueillir

* Remerciez le client pour l'entretien
* Expliquez que vous êtes prêts à lancer le service et que vous voudriez montrer le produit.
* Expliquez que si le prospect est intéressé vous pourrez lui permettre d’accéder au produit en avant-première

# Présenter la proposition de valeur unique

* Peut-être la page d'accueil ?

# Présenter les prix

* Proposer de naviguer librement sur le site ou d’utiliser le jeu ou le produit
* Lorsque le client arrive sur la page des prix, demandez-lui ce qu’il pense de la tarification.

# Inscription et activation

**Ceci est le coeur de l’entretien**

Demandez au prospect de s’inscrire et observer la façon dont il
navigue à travers le flux d’activation

Exemples :

* Inscription à la version alpha pour le download
* Création d’un compte
* Acheter le produit

# Questions de Kano

Après avoir identifié une partie des futurs fonctionnalités, utiliser les questions de Kano pour identifier les prochaines fonctionnalités que vous implémenterez.

# Terminer l’entretien

* Le prospect a certainement été jusqu’au bout
* On dispose d’une liste de problèmes d’utilisabilité à résoudre

**BRAVO votre premier utilisateur est inscrit !**

# Documenter l'entretien

* Retranscrivez immédiatement les résultats
* Créez un modèle
* Mettre à jour vos questionnaire
* Mettre à jour le lean canvas.
* Utilisez une suite CRM

# 

![](../includes/canvas-entretien-mvp.svg)

#

:::: {.columns}
::: {.column width="30%"}
:::
::: {.column width="70%"}

![](../includes/logo-azae.svg){height=100px}

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tclavier@azae.net |
|<i class="fa fa-mastodon"></i>   | @thomas@libre-entreprise.com |
|<i class="fa fa-matrix-org"></i> | @tclavier:matrix.org |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |

:::
::::

Support de formation créé par [Efidev](https://www.efidev.com/) et [![](../includes/logo-azae.svg){height=1.5em}](https://azae.net) sous licence [![](../includes/cc-by-sa.svg){height=1em}](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

